"""
hello.py

Basic example click https://github.com/mitsuhiko/click application

Notes:

* Adding Parameters
    * Use option() and argument() decorators to add parameters

* @click.command() decorator converts the decorated function into a
   click click.Command (http://click.pocoo.org/5/api/#click.Command) object

* Nesting
    * Commands can be attached to other commands of type 'click.Group'
    * Allows arbitrary nesting of scripts

* options are optional and always contain '--'

* arguments are optional, but mandatory by default
    * do not have help text
    * should be documented as part of the string
"""
import click


class Config(object):
    """
    This object is used to handle state between click commands & groups
    """
    def __init__(self):
        self.verbose = False


"""Makes a decorator which passes a click config object to each of the click callbacks"""
pass_config = click.make_pass_decorator(Config, ensure=True)


@click.group()
@click.option('--verbose', is_flag=True)
@click.option('--home-directory', type=click.Path())
@pass_config
def cli(config, verbose, home_directory):
    config.verbose = verbose
    if home_directory is None:
        home_directory = '.'
    config.home_directory = home_directory

@cli.command()
@click.option('--string', default='World',
              help='This is the thing that is greeted.')
@click.option('--repeat', default=1, type=int,
              help='How many times you should be greeted.')

# default of '-' for file results in stdin
@click.argument('out', type=click.File('w'), default='-', required=False)
@pass_config
def say(config, string, repeat, out):
    """This script greets you."""
    click.echo(out)

    if config.verbose:
        click.echo('We are in verbose mode.')
    click.echo('Home directory is %s' % config.home_directory)
    for x in xrange(repeat):
        click.echo('Hello %s' % string, file=out)
